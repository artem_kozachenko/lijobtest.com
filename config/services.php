<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID', '1095490475815-0e9hvb0f2m79lpss7t2j1scn4ab45nei.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET', '4omthX7KLbeY4F9DC7h55OsA'),
        'redirect' => env('GOOGLE_REDIRECT_URI', 'http://lijobtest.com/login/google/callback'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID', '2241761792716816'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET', 'f37004386c6d3ed4dbb4f07a8ee527f3'),
        'redirect' => env('FACEBOOK_REDIRECT_URI', 'http://lijobtest.com/login/facebook/callback'),
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];
