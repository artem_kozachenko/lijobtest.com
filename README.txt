﻿1. Установить все необходимые фалйы
    composer install
2. Создать базу данных
3. Создать и настоить .env из .env.example
4. Очистить кэш настроек
    php artisan config:clear
5. Сгенерировать ключ
    php artisan key:generate
6. Создать талицы в базе данных и заполнить их тестовыми данными
    php artisan migrate --seed
