<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Google authentication
Route::get('login/google', 'Auth\GoogleController@redirectToProvider')->name('googleLogin');
Route::get('login/google/callback', 'Auth\GoogleController@handleProviderCallback');
// Facebook authentication
Route::get('login/facebook', 'Auth\FacebookController@redirectToProvider')->name('facebookLogin');
Route::get('login/facebook/callback', 'Auth\FacebookController@handleProviderCallback');
// Logout
Route::match(['get', 'post'], 'wall/logout', 'WallController@logout')->name('logout');
// Comment actions
Route::match(['get', 'post'], 'wall/add_comment', 'CommentController@addComment')->name('addComment');
Route::match(['get', 'post'], 'wall/add_message', 'CommentController@addMessage')->name('addMessage');
Route::match(['get', 'post'], 'wall/edit_comment', 'CommentController@edit')->name('editComment');
// Wall page
Route::get('/wall', 'WallController@show')->name('wall');
// Authentication page
Route::get('/', function () {
    return view('log_in');
})->name('login')->middleware('guest');
