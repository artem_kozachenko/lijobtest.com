/*
1. Show form for editing messages/comments.
2. Close all other forms.
3. Focus on input.
4. Set input[name="id"] as message/comment id.
5. Set input[name="edit"] as message/comment text.
*/
$('i[name="edit-icon"]').click(function() {
    $('#add_message').hide();
    $('#add_comment').hide();
    $('#edit').show();
    $('#edit>div>input[name="edit"]').focus();
    var value = $(this).attr('value');
    var text = $(this).parents('#comment-text-' + value + '').children('div>div[class="text"]').text();
    $('#edit>input[name="id"]').attr('value', value);
    $('#edit>div>input[name="edit"]').val(text);
});
/*
1. Show form for adding comments.
2. Close all other forms.
3. Focus on input.
4. Set input[name="parent_id"] as parent message id.
5. Set input[name="comment_level"] as parent comment level + 1.
*/
$('i[name="comment-icon"]').click(function() {
    $('#add_message').hide();
    $('#edit').hide();
    $('#add_comment').show();
    $('#add_comment>div>input[name="message_comment"]').focus();
    var value = $(this).attr('value');
    var blank_index = value.indexOf(' ');
    var parent_id = value.substring(0, blank_index);
    var comment_level = value.slice(blank_index, );
    comment_level = parseInt(comment_level) + 1;
    $('#add_comment>input[name="parent_id"]').attr('value', parent_id);
    $('#add_comment>input[name="comment_level"]').attr('value', comment_level);
});
// Hide side forms and show main
$('#close_add_comment').click(function() {
    $('#add_message').show();
    $('#add_comment').hide();
});
$('#close_edit_comment').click(function() {
    $('#add_message').show();
    $('#edit').hide();
});
