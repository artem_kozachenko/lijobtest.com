<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'user_id', 'parent_id', 'message', 'comment_level',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
