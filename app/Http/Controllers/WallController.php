<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Comment;

class WallController extends Controller
{
    public function show()
    {
        $comments = Comment::join('users', 'users.id', '=', 'comments.user_id')
            ->select(
                'users.name',
                'comments.id',
                'comments.user_id',
                'comments.parent_id',
                'comments.message',
                'comments.comment_level',
                'comments.created_at')
            ->get();
        return view('wall', ['comments' => $this->makeArray($comments)]);
    }

    protected function makeArray($comments)
    {
        $children = [];
        foreach($comments as $comment) {
			$children[$comment->parent_id][] = $comment;
		}
        foreach($comments as $comment) {
			if(isset($children[$comment->id])) {
                $comment->children = $children[$comment->id];
            }
		}
        if(count($children) > 0) {
			$tree = $children[0];
		}
		else {
			$tree = [];
		}
        krsort($tree);
		return $tree;
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
