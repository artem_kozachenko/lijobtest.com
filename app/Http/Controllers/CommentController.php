<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    public function addMessage(Request $request)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);
        Comment::create([
            'user_id' => Auth::user()->id,
            'parent_id' => $request->input('parent_id'),
            'message' => $request->input('message'),
            'comment_level' => $request->input('comment_level')
        ]);
        return redirect()->back();
    }

    public function addComment(Request $request)
    {
        $this->validate($request, [
            'message_comment' => 'required',
        ]);
        Comment::create([
            'user_id' => Auth::user()->id,
            'parent_id' => $request->input('parent_id'),
            'message' => $request->input('message_comment'),
            'comment_level' => $request->input('comment_level')
        ]);
        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'edit' => 'required',
        ]);
        Comment::where('id', $request->input('id'))
            ->update(['message' => $request->input('edit')]);
        return redirect()->back();
    }
    
}
