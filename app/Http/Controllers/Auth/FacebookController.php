<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class FacebookController extends Controller
{
    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        if(User::where('email', $user->email)->exists()) {
            $user = User::where('email', $user->email)->first();
            Auth::login($user);
            return redirect()->route('wall');
        }
        else {
            $user = User::create([
                'name' => $user->name,
                'email' => $user->email,
            ]);
            Auth::login($user);
            return redirect()->route('wall');
        }
    }
}
