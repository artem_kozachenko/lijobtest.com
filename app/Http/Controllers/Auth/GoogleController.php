<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class GoogleController extends Controller
{
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Goole.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('google')->user();
        if(User::where('email', $user->email)->exists()) {
            $user = User::where('email', $user->email)->first();
            Auth::login($user);
            return redirect()->route('wall');
        }
        else {
            $user = User::create([
                'name' => $user->name,
                'email' => $user->email,
            ]);
            Auth::login($user);
            return redirect()->route('wall');
        }
    }
}
