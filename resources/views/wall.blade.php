<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Wall</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adminlte/dist/css/skins/skin-blue.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/wall.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    </head>
    <body class="hold-transition skin-blue ">
            <div class="wrapper">
                <header class="main-header">
                    <a class="logo">
                        <span class="logo-lg"><b>LI</b>JobTest</span>
                    </a>
                    <nav class="navbar navbar-static-top">
                        <div class="navbar-custom-menu">
                            @if(Auth::check())
                                <ul class="nav navbar-nav">
                                    <li class="dropdown user user-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="hidden-xs">{{ Auth::user()->email }}</span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="user-footer">
                                                <form method="post" action="{{ route('logout') }}">
                                                    {{ csrf_field() }}
                                                    <button class="btn btn-default btn-flat user-pannel-buttons" type="submit" name="button">Sign out</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            @else
                                <a href="/" class="btn btn-default btn-flat sign-in a" type="submit" name="button">Sign in</a>
                            @endif
                        </div>
                    </nav>
                </header>
                <div class="content-wrapper a">
                    <div class="box-footer box-comments">
                        @foreach($comments as $comment)
                            <div class="box-comment">
                                <div id="comment-text-{{ $comment->id }}" class="comment-text level-{{ $comment->comment_level }}">
                                    <span class="username">
                                        {{ $comment->name }}
                                        <span class="text-muted pull-right">{{ $comment->created_at }}</span>
                                        <br>
                                        @if(Auth::check())
                                            <i name="comment-icon" class="text-muted pull-right fa fa-comments action" value="{{ $comment->id . ' ' . $comment->comment_level }}"></i>
                                            @if(Auth::user()->id == $comment->user_id)
                                                <i name="edit-icon" class="text-muted pull-right fa fa-pencil action" value="{{ $comment->id }}"></i>
                                            @endif
                                        @endif
                                    </span>
                                    <div class="text">{{ $comment->message }}</div>
                                </div>
                            </div>
                            @if(isset($comment->children))
                            	@include('rotator', ['comments'=> $comment->children])
                            @endif
                        @endforeach
                    </div>
                    <div class="box-footer">
                        @if(Auth::check())
                            <!-- Form for adding messages -->
                            @if($errors->has('message_comment') || $errors->has('edit'))
                                <form style="display:none;" id="add_message" name="add_message" action="{{ route('addMessage') }}" method="post">
                            @else
                                <form id="add_message" name="add_message" action="{{ route('addMessage') }}" method="post">
                            @endif
                                {{ csrf_field() }}
                                <input name="parent_id" type="hidden" value="0">
                                <input name="comment_level" type="hidden" value="0">
                                <div class="{{ $errors->has('message') ? ' has-error' : '' }}">
                                    <input name="message" type="text" class="form-control input-sm" placeholder="Press enter to write message">
                                </div>
                            </form>
                            <!-- Form for adding comments -->
                            <form {{ $errors->has('message_comment') ? ' ' : 'style=display:none;' }} name="add_comment" action="{{ route('addComment') }}" id="add_comment" method="post">
                                {{ csrf_field() }}
                                <input name="parent_id" type="hidden" value="{{ old('parent_id') }}">
                                <input name="comment_level" type="hidden" value="{{ old('comment_level') }}">
                                <div class="input-group {{ $errors->has('message_comment') ? ' has-error' : '' }}">
                                    <input name="message_comment" type="text" class="form-control input-sm" placeholder="Press enter to write comment">
                                    <span id="close_add_comment" class="input-group-btn">
                                        <span name="search"  class="btn btn-flat">
                                            <i class="fa fa-times back"></i>
                                        </span>
                                    </span>
                                </div>
                            </form>
                            <!-- Form for editing comments/messages -->
                            <form {{ $errors->has('edit') ? ' ' : 'style=display:none;' }} name="edit" action="{{ route('editComment') }}" id="edit" method="post">
                                {{ csrf_field() }}
                                <input name="id" type="hidden" value="{{ old('id') }}">
                                <div class="input-group {{ $errors->has('edit') ? ' has-error' : '' }}">
                                    <input name="edit" type="text" class="form-control input-sm" placeholder="Press enter to edit comment/message">
                                    <span id="close_edit_comment" class="input-group-btn">
                                        <span name="search"  class="btn btn-flat">
                                            <i class="fa fa-times back"></i>
                                        </span>
                                    </span>
                                </div>
                            </form>
                        @else
                            <!-- Unauthorized user form-->
                            <form action="#" method="post">
                                <input disabled type="text" class="form-control input-sm" placeholder="You have to sign in to send messages.">
                            </form>
                        @endif
                    </div>
                </div>
            </div>
            <!-- jQuery 3 -->
            <script src="{{ URL::asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
            <!-- Bootstrap 3.3.7 -->
            <script src="{{ URL::asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
            <!-- Forms scripts -->
            <script src="{{ URL::asset('js/forms.js') }}"></script>
    </body>
</html>
