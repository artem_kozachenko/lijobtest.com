@foreach($comments as $comment)
    <div class="comment child">
        <div class="box-comment">
            <div id="comment-text-{{ $comment->id }}" class="comment-text level-{{ $comment->comment_level }}">
                <span class="username">
                    {{ $comment->name }}
                    <span class="text-muted pull-right">{{ $comment->created_at }}</span>
                    <br>
                    @if(Auth::check())
                        <i name="comment-icon" class="text-muted pull-right fa fa-comments action" value="{{ $comment->id . ' ' . $comment->comment_level }}"></i>
                        @if(Auth::user()->id == $comment->user_id)
                            <i name="edit-icon" class="text-muted pull-right fa fa-pencil action" value="{{ $comment->id }}"></i>
                        @endif
                    @endif
                </span>
                <div class="text">{{ $comment->message }}</div>
            </div>
        </div>
        @if(isset($comment->children))
        	@include('rotator', ['comments' => $comment->children])
        @endif
    </div>
@endforeach
