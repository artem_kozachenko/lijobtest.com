<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/dist/css/AdminLTE.min.css') }}">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-box-body" >
                <div class="social-auth-links text-center">
                    <a href="{{ route('facebookLogin') }}" class="btn btn-block btn-social btn-facebook btn-flat">
                        <i class="fa fa-facebook"></i> Sign in using Facebook
                     </a>
                    <a href="{{ route('googleLogin') }}" class="btn btn-block btn-social btn-google btn-flat">
                        <i class="fa fa-google-plus"></i> Sign in using Google+
                    </a>
                    <!-- <a href="#" class="btn btn-block btn-social btn-vk btn-flat">
                        <i class="fa fa-vk"></i> Sign in using VK
                     </a> -->
                </div>
            </div>
        </div>
    </body>
</html>
