<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 5; $i++) {
            $commentsCollection[$i] = factory(App\Comment::class)->create([
                'user_id' => function() {
                    return factory(App\User::class)->create()->id;
                },
                'parent_id' => 0,
                'comment_level' => 0,
                'message' => file_get_contents('http://loripsum.net/api/1/short/plaintext'),
            ]);
        }
        $i = 0;
        foreach ($commentsCollection as $key => $value) {
            $comments[$i]['id'] = $value->id;
            $comments[$i]['comment_level'] = $value->comment_level;
            $i++;
        }
        for ($i = 0; $i < 20; $i++) {
            $max = count($comments);
            $index = rand(0, $max - 1);
            $new_comment = factory(App\Comment::class)->create([
                'user_id' => function() {
                    return factory(App\User::class)->create()->id;
                },
                'parent_id' => $comments[$index]['id'],
                'comment_level' => $comments[$index]['comment_level'] + 1,
                'message' => file_get_contents('http://loripsum.net/api/1/short/plaintext'),
            ]);
            $comments[$max]['id'] = $new_comment->id;
            $comments[$max]['comment_level'] = $new_comment->comment_level;
        }
    }
}
